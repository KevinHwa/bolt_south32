import pkg_resources

from tropofy.polymathian.config import PolyTropofyAppTestConfig

from bolt_south32.config.base import BoltSouth32Config


class TestBoltConfig(BoltSouth32Config, PolyTropofyAppTestConfig):
    def get_test_data_set(self):
        return pkg_resources.resource_filename("bolt_south32", "test/data/base.trp")

    def get_redis_config(self):
        return False

    def get_test_user_groups(self):
        return [
            {
                "name": "bolt_demo_developers",
                "roles": [
                    {"name": "bolt_demo_master"},
                    {"name": "bolt_demo_marketing"},
                    {"name": "bolt_demo_mining"},
                    {"name": "bolt_demo_shipping"},
                    {"name": "bolt_demo_write_users"},
                ],
            },
            {
                "name": "bolt_demo_users",
                "roles": [
                    {"name": "bolt_demo_logistics"},
                    {"name": "bolt_demo_marketing"},
                    {"name": "bolt_demo_mining"},
                    {"name": "bolt_demo_shipping"},
                    {"name": "bolt_demo_write_users"},
                ],
            },
        ]

    def get_db_name(self):
        return "tropofy_app_tests"


apps_config = TestBoltConfig().get_apps_config()
