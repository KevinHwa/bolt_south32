from bolt_base.apps.configuration import DockerConfigMixin

from bolt_south32.config.base import BoltSouth32Config


class DockerConfig(DockerConfigMixin, BoltSouth32Config):
    pass


demo_config = DockerConfig()
apps_config = demo_config.get_apps_config()
