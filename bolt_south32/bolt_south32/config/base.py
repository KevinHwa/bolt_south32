import os
import pkg_resources

from bolt_base.apps.configuration import BoltBaseConfig


class BoltSouth32Config(BoltBaseConfig):
    def get_project_name(self) -> str:
        return "bolt_south32"

    def get_port(self):
        return os.getenv("BOLT_SOUTH32_APP_SERVER_PORT", 8090)

    def get_alembic_config(self):
        bolt_root = os.path.join(os.path.dirname(__file__), "..")
        base_alembic_script_location = pkg_resources.resource_filename("bolt_base.models.db_management", "alembic")
        local_alembic_script_location = os.path.join(bolt_root, "models", "db_management", "alembic")
        return {"alembic": [base_alembic_script_location, local_alembic_script_location]}

    def get_apps(self):
        apps = [
            {
                "module": "bolt_south32.apps.data_app_def",
                "classname": "DataApp",
                "config": {
                    "key.public": "dS4qoZU3h5uS5304dnMHqoIPUUt0dk0CyT3RX58kSZsXtioHiEw3y7U7LATYYffF",
                    "key.private": "J8vGDG4QpiEUTHU8gb9kUfIfj8P2iDUz8uYqUScM4M2XV9HAhLOCqvYYBhO3JwKW",
                    "name": "South32 Data",
                    "url_name": "bolt_south32_data",
                },
            },
            {
                "module": "bolt_south32.apps.tactical_optimiser_app_def",
                "classname": "TacticalApp",
                "config": {
                    "key.public": "47WNPB0RpJsHZBMRJC0rUx0ASY2WQA0ZuYcLLOC9wxkR5NvyxXYZkVz4spiK57XK",
                    "key.private": "APS6mmsE30L5Hzc3M0wFJeGNYdGvhy86PRgXMV5EhzWkr6BOG5aXfBY8t3arjG2C",
                    "name": "South32 Tactical Optimiser",
                    "url_name": "bolt_south32_tactical",
                },
            },
            {
                "module": "bolt_south32.apps.operational_optimiser_app_def",
                "classname": "OperationalApp",
                "config": {
                    "key.public": "YfKdlQw4OiEVi0dbQ6ajJHHgnzgZYYyMKflH1sbQpTjbG2yE9QEaf4hLHAmSixFA",
                    "key.private": "PvV0kbTS53B8UJqze6K7WeyVvST29jcy4mycLgXQDHoerYZsQwGVhAYbYRi0MVIq",
                    "name": "South32 Operational Optimiser",
                    "url_name": "bolt_south32_operational",
                },
            },
        ]

        for app in apps:
            app["config"]["master_data.key.public"] = "dS4qoZU3h5uS5304dnMHqoIPUUt0dk0CyT3RX58kSZsXtioHiEw3y7U7LATYYffF"
            app["config"]["aws_s3_bucket"] = "tropofy"

        return apps
