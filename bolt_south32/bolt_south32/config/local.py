from bolt_south32.config.base import BoltSouth32Config


class BoltSouth32LocalConfig(BoltSouth32Config):
    def get_aws_s3_usercontent_bucket(self):
        return None


south32_local_config = BoltSouth32LocalConfig()
apps_config = south32_local_config.get_apps_config()
