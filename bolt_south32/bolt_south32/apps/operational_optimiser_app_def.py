import collections
import datetime as dt
import logging

from dateutil import relativedelta as rd

from bolt_base.apps.optimiser_app_def import BoltOptimiserApp
from bolt_base.models import orm, parameters
from bolt_base.utils import time_step as time_step_utils

from bolt_south32.apps._base import South32AppMixin
from bolt_south32.apps.south32_tab_factory import (
    OperationalInputTabFactory,
    OperationalOptimiserTabFactory,
    OperationalOutputTabFactory,
)

from typing import List, TYPE_CHECKING

if TYPE_CHECKING:
    from tropofy.app.meta import AppSession
    from tropofy.app.data_set import DataSet
    from tropofy.ext.datetime_window import DateTimeWindow


class OperationalApp(BoltOptimiserApp, South32AppMixin):
    @classmethod
    def get_start_datetime(cls, data_set: "DataSet") -> dt.datetime:
        return parameters.BoltOptimisationParameters.optimisation_start_time.get_value(data_set)

    @classmethod
    def get_end_datetime(cls, data_set: "DataSet") -> dt.datetime:
        return parameters.BoltOptimisationParameters.optimisation_end_time.get_value(data_set)

    @classmethod
    def get_time_steps(
        cls, data_set: "DataSet", start_datetime: dt.datetime, end_datetime: dt.datetime
    ) -> List["DateTimeWindow"]:
        interval: rd.relativedelta = parameters.BoltOptimisationParameters.optimisation_step_interval.get_value(
            data_set
        )
        return time_step_utils.get_uniform_period_time_steps_truncated_at_end_time(
            start_datetime, end_datetime, interval
        )

    def get_default_end(self, app_session: "AppSession") -> dt.datetime:
        # now + 3 months
        return dt.datetime.now() + rd.relativedelta(months=3)

    def max_optimisation_length(self, app_session: "AppSession") -> rd.relativedelta:
        return rd.relativedelta(months=6)

    def get_optimisation_dates(self, app_session, master_data_set, requested_start, requested_end):
        form_start = requested_start

        start_shift: orm.Shift = (
            master_data_set.query(orm.Shift)
            .filter(orm.Shift.shift_name == parameters.DemoParameters.start_shift.get_value(master_data_set))
            .all()[0]
        )

        end_shift: orm.Shift = (
            master_data_set.query(orm.Shift)
            .filter(orm.Shift.shift_name == parameters.DemoParameters.end_shift.get_value(master_data_set))
            .all()[0]
        )

        start = dt.datetime(
            form_start.year,
            form_start.month,
            form_start.day,
            start_shift.start_time.hour,
            start_shift.start_time.minute,
            start_shift.start_time.second,
        )
        optimisation_end = truncated_end = requested_end
        optimisation_end = dt.datetime.combine(optimisation_end, end_shift.start_time)
        return collections.namedtuple("Dates", ["optimisation_start", "optimisation_end", "truncated_end"])(
            start, optimisation_end, truncated_end
        )

    def post_process_data_set(self, app_session: "AppSession", master_data_set, logger: logging.Logger, **kwargs):
        parameters.BoltOptimisationParameters.optimisation_step_interval.set_value(
            app_session.data_set, rd.relativedelta(days=1)
        )
        parameters.NonEngineParameters.integer_trains.set_value(app_session.data_set, True)
        parameters.NonEngineParameters.integer_shiploaders.set_value(app_session.data_set, True)
        parameters.NonEngineParameters.trains_utilisation.set_value(app_session.data_set, True)

    def get_table_names_not_to_copy_from_master_data(self):
        return [
            orm.TakeOrPayContract.__table__.name,
            orm.TakeOrPayRoute.__table__.name,
            orm.ContractedDemand.__table__.name,
            orm.OpportunisticDemand.__table__.name,
        ] + super().get_table_names_not_to_copy_from_master_data()

    def _get_auto_load_data_set_id(self, app_session: "AppSession"):
        return None

    # noinspection PyTypeChecker
    def get_tab_factories(self):
        return [
            OperationalInputTabFactory(),
            OperationalOptimiserTabFactory(),
            OperationalOutputTabFactory(),
        ] + super().get_tab_factories()
