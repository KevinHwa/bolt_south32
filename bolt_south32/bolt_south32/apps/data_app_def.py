import datetime as dt

from dateutil import relativedelta as rd

from tropofy.app.data_set import DataSet
from tropofy.ext.datetime_window import DateTimeWindow

from bolt_base.apps.base_tab_factory import TabFactory
from bolt_base.models import orm, parameters  # required for export data set to work
from bolt_base.apps.data_app_def import BoltDataApp
from bolt_base.utils import time_step as time_step_utils
from bolt_base.apps.base_tab_factory import DataSummaryTabFactory

from bolt_south32.apps._base import South32AppMixin
from bolt_south32.apps.south32_tab_factory import (
    LogisticsTabFactory,
    MarketingTabFactory,
    MiningTabFactory,
    ShippingTabFactory,
)


from typing import List


class DataApp(BoltDataApp, South32AppMixin):
    @classmethod
    def get_start_datetime(cls, data_set: DataSet) -> dt.datetime:
        return dt.datetime.now() - rd.relativedelta(years=10)

    @classmethod
    def get_end_datetime(cls, data_set: DataSet) -> dt.datetime:
        return dt.datetime.now() + rd.relativedelta(years=10)

    @classmethod
    def get_time_steps(
        cls, data_set: DataSet, start_datetime: dt.datetime, end_datetime: dt.datetime
    ) -> List[DateTimeWindow]:
        interval: rd.relativedelta = parameters.BoltOptimisationParameters.optimisation_step_interval.get_value(
            data_set
        )
        return time_step_utils.get_uniform_period_time_steps(start_datetime, end_datetime, interval)

    def get_tab_factories(self) -> List[TabFactory]:
        return super().get_tab_factories() + [
            MiningTabFactory(),
            LogisticsTabFactory(),
            ShippingTabFactory(),
            MarketingTabFactory(),
            DataSummaryTabFactory(),
        ]
