from tropofy.app.navigation import step_tab, step_group, step

from bolt_base.apps.base_tab_factory import (
    DataPortalTabFactory,
    OptimiserTabFactory,
    MasterDataTabFactory,
    InputTabFactory,
    OutputTabFactory,
)
from bolt_base.apps.widgets import grouped_charts as gc, reports
from bolt_base.apps import widgets as base_wg
from bolt_base.models import orm as base_orm
from bolt_base.apps.navigation import card

from bolt_base_demo.apps.widgets import exporter, uploader

from typing import List, cast, TYPE_CHECKING

if TYPE_CHECKING:
    from tropofy.app.meta import AppSession
    from tropofy.app.navigation import Step, StepTab, StepGroup


class South32MasterTabFactory(MasterDataTabFactory):
    ROLE: str = "bolt_south32_master"


# Data App
class LogisticsTabFactory(DataPortalTabFactory):
    ROLE: str = "bolt_south32_logistics"

    def get_tab_name(self) -> str:
        return "Logistics"

    @classmethod
    def get_static_steps(cls, app_session: "AppSession") -> List["Step"]:
        return [
            step(
                "Locations",
                cards=cls.bolt_grid_group(
                    base_orm.Pit,
                    base_orm.Mine,
                    base_orm.Crusher,
                    base_orm.ProcessingPlant,
                    base_orm.TrainLoadOut,
                    base_orm.RailLoop,
                    base_orm.RailingTerminal,
                    base_orm.ShippingTerminal,
                    base_orm.Port,
                ),
            ),
            step("Entity", cards=cls.bolt_grid_group(base_orm.Entity)),
            step("Alias", cards=cls.bolt_grid_group(base_orm.Alias)),
        ]

    @classmethod
    def get_charts_steps(cls, app_session: "AppSession") -> List["Step"]:
        from bolt_base.apps.optimiser_app_def import BoltOptimiserApp

        app_ = cast(BoltOptimiserApp, app_session.app)
        return [
            step("Network Charts", cards=[gc.get_network_chart_group()]),
            step("Opening Stocks Charts", cards=[gc.get_opening_stocks_charts_group()]),
            step("Stockyard Charts", cards=[gc.get_stockyard_pair_chart_group()]),
            step(
                "Availability Charts",
                cards=[
                    gc.combine_chart_groups(
                        app_session,
                        gc.get_processing_plant_avail_chart_group(
                            app_.limit_time_to_optimisation_bounds, show_chart_title=True
                        ),
                        gc.get_vehicle_avail_chart_group(show_chart_title=True),
                        title="Availability Charts",
                    )
                ],
            ),
        ]

    def get_gui_tab(self, app_session: "AppSession") -> "StepTab":
        return step_tab(
            name=self.get_tab_name(),
            step_groups=[
                step_group(name="Statics", steps=self.get_static_steps(app_session)),
                step_group(name="Network", steps=self.get_network_steps(app_session)),
                step_group(name="Clean and Terminal Stockyards", steps=self.get_stockyards_steps(app_session)),
                step_group(name="Opening Stocks", steps=self.get_opening_stocks_steps(app_session)),
                step_group(name="Take or Pay Contracts", steps=self.get_take_or_pay_contracts_steps(app_session)),
                step_group(name="Throughput Limits", steps=self.get_throughput_limits_steps(app_session)),
                step_group(name="Charts", steps=self.get_charts_steps(app_session)),
            ],
        )


class MarketingTabFactory(DataPortalTabFactory):
    ROLE: str = "bolt_south32_marketing"

    def get_tab_name(self) -> str:
        return "Marketing"

    @classmethod
    def get_static_steps(cls, app_session: "AppSession") -> List["Step"]:
        return [step("Alias", cards=cls.bolt_grid_group(base_orm.Alias))]

    @classmethod
    def get_charts_steps(cls, app_session: "AppSession") -> List["Step"]:
        return [
            step(
                "Demand Charts",
                cards=[
                    gc.combine_chart_groups(
                        app_session,
                        [base_wg.DemandSubplotChart(show_chart_title=True)],
                        gc.get_vessel_demand_chart_group(show_chart_title=True),
                        gc.get_contracted_demand_chart_group(show_chart_title=True),
                        gc.get_opportunistic_demand_chart_group(show_chart_title=True),
                        title="Demand Charts",
                    )
                ],
            ),
            step("Supply vs Demand Charts", cards=[gc.get_supply_vs_demand_chart_group()]),
            step("Pricing Charts", cards=[gc.get_pricing_chart_group()]),
        ]

    def get_gui_tab(self, app_session: "AppSession") -> "StepTab":
        return step_tab(
            name=self.get_tab_name(),
            step_groups=[
                step_group(name="Statics", steps=self.get_static_steps(app_session)),
                step_group(name="Pricing", steps=self.get_pricing_steps(app_session)),
                step_group(name="Contracts", steps=self.get_contracts_steps(app_session)),
                step_group(name="Blending", steps=self.get_blending_steps(app_session)),
                step_group(name="Charts", steps=self.get_charts_steps(app_session)),
            ],
        )


class MiningTabFactory(DataPortalTabFactory):
    ROLE: str = "bolt_south32_mining"

    def get_tab_name(self) -> str:
        return "Mining"

    @classmethod
    def get_static_steps(cls, app_session: "AppSession") -> List["Step"]:
        return [
            step(
                "Attributes and Basis",
                cards=cls.bolt_grid_group(base_orm.Attribute, base_orm.Basis, base_orm.AttributeBasis),
            ),
            step(
                "Locations",
                cards=cls.bolt_grid_group(
                    base_orm.Pit,
                    base_orm.Mine,
                    base_orm.Crusher,
                    base_orm.ProcessingPlant,
                    base_orm.TrainLoadOut,
                    base_orm.RailLoop,
                    base_orm.RailingTerminal,
                    base_orm.ShippingTerminal,
                    base_orm.Port,
                ),
            ),
            step("Alias", cards=cls.bolt_grid_group(base_orm.Alias)),
        ]

    @classmethod
    def get_processing_steps(cls, app_session: "AppSession") -> List["Step"]:
        return [
            step(
                "Configuration",
                cards=cls.bolt_grid_group(
                    base_orm.ProcessingConfiguration,
                    base_orm.ProcessingConfigurationType,
                    base_orm.ProcessingConfigurationMineCommodity,
                ),
            ),
            step("Block Yield", cards=cls.bolt_grid_group(base_orm.BlockYield)),
            step("Block Attributes", cards=cls.bolt_grid_group(base_orm.BlockAttribute)),
            step("Plant Availability", cards=cls.bolt_grid_group(base_orm.LocationAvailability)),
        ]

    @classmethod
    def get_charts_steps(cls, app_session: "AppSession") -> List["Step"]:
        from bolt_base.apps.optimiser_app_def import BoltOptimiserApp

        app_ = cast(BoltOptimiserApp, app_session.app)
        return [
            step("Mining Charts", cards=[gc.get_tonnes_per_period_chart_group(app_.limit_time_to_optimisation_bounds)]),
            step("Block Attributes Charts", cards=[gc.get_block_attributes_chart_group()]),
            step("Processing Attributes Charts", cards=[gc.get_processing_attributes_chart_group()]),
            step("Stockyard Charts", cards=[gc.get_stockyard_pair_chart_group()]),
            step(
                "Availability Charts",
                cards=[
                    gc.combine_chart_groups(
                        app_session,
                        gc.get_processing_plant_avail_chart_group(
                            app_.limit_time_to_optimisation_bounds, show_chart_title=True
                        ),
                        gc.get_vehicle_avail_chart_group(show_chart_title=True),
                        title="Availability Charts",
                    )
                ],
            ),
        ]

    @classmethod
    def get_mine_blocks_steps(cls, app_session: "AppSession") -> List["Step"]:
        block_uploader = uploader.BlockDataUploader()
        return [
            step(
                "Blocks",
                cards=[
                    card(block_uploader),
                    card(exporter.BlockDataExporter()),
                    card(base_wg.BoltGrid(base_orm.Block, non_embedded_upload_widgets=[block_uploader])),
                    card(base_wg.BoltGrid(base_orm.BlockAttribute, non_embedded_upload_widgets=[block_uploader])),
                    card(
                        base_wg.BoltGrid(
                            base_orm.MineCommodityMineProductMapping, non_embedded_upload_widgets=[block_uploader]
                        )
                    ),
                ],
                help_markdown="Block Uploader/Exporter  \n"
                "Block Uploader requires sheet with the following tabs:"
                " ['block', 'blockattribute', 'blocksupply', 'blockyield']",
            ),
            step("Block Supply", cards=cls.bolt_grid_group(base_orm.BlockSupply)),
            step("Blocks Without Attributes", cards=[card([base_wg.BlocksWithoutAttributesGrid()])]),
        ]

    def get_gui_tab(self, app_session: "AppSession") -> "StepTab":
        return step_tab(
            name=self.get_tab_name(),
            step_groups=[
                step_group(name="Statics", steps=self.get_static_steps(app_session)),
                step_group(name="Mine Blocks", steps=self.get_mine_blocks_steps(app_session)),
                step_group(name="Processing", steps=self.get_processing_steps(app_session)),
                step_group(name="Raw Stockyards", steps=self.get_stockyard_steps(app_session)),
                step_group(name="Charts", steps=self.get_charts_steps(app_session)),
            ],
        )


class ShippingTabFactory(DataPortalTabFactory):
    ROLE: str = "bolt_south32_shipping"

    def get_tab_name(self) -> str:
        return "Shipping"

    @classmethod
    def get_static_steps(cls, app_session: "AppSession") -> List["Step"]:
        return [step("Alias", cards=cls.bolt_grid_group(base_orm.Alias))]

    @classmethod
    def get_charts_steps(cls, app_session: "AppSession") -> List["Step"]:
        return [
            step("Planned Vessel Arrivals Chart", cards=[gc.get_vessel_gantt_chart_group()]),
            step("Sales Order Chart", cards=[gc.sales_order_chart_group()]),
            step(
                "Demand Charts",
                cards=[
                    gc.combine_chart_groups(
                        app_session,
                        [base_wg.DemandSubplotChart(show_chart_title=True)],
                        gc.get_vessel_demand_chart_group(show_chart_title=True),
                        title="Demand Charts",
                    )
                ],
            ),
        ]

    def get_gui_tab(self, app_session: "AppSession") -> "StepTab":
        return step_tab(
            name=self.get_tab_name(),
            step_groups=[
                step_group(name="Statics", steps=self.get_static_steps(app_session)),
                step_group(name="Shipping Data", steps=self.get_shipping_steps(app_session)),
                step_group(name="Charts", steps=self.get_charts_steps(app_session)),
            ],
        )


# Operational App
class OperationalInputTabFactory(InputTabFactory):
    @classmethod
    def get_input_chart_steps(cls, app_session: "AppSession") -> List["Step"]:
        from bolt_base.apps.optimiser_app_def import BoltOptimiserApp

        app_ = cast(BoltOptimiserApp, app_session.app)
        return [
            step("Network Charts", cards=[gc.get_network_chart_group()]),
            step("Mining Charts", cards=[gc.get_tonnes_per_period_chart_group(app_.limit_time_to_optimisation_bounds)]),
            step("Opening Stocks Charts", cards=[gc.get_opening_stocks_charts_group()]),
            step("Stockyard Charts", cards=[gc.get_stockyard_pair_chart_group()]),
            step("Block Attributes Charts", cards=[gc.get_block_attributes_chart_group()]),
            step("Processing Attributes Charts", cards=[gc.get_processing_attributes_chart_group()]),
            step("Processing Yield Charts", cards=[gc.get_processing_yield_chart_group()]),
            step("Pricing Charts", cards=[gc.get_pricing_chart_group()]),
            step("Sales Order Charts", cards=[gc.sales_order_chart_group()]),
            step(
                "Demand Charts",
                cards=[
                    gc.combine_chart_groups(
                        app_session,
                        [base_wg.DemandSubplotChart(show_chart_title=True)],
                        gc.get_vessel_demand_chart_group(show_chart_title=True),
                        title="Demand Charts",
                    )
                ],
            ),
            step("Planned Vessel Arrivals Chart", cards=[gc.get_vessel_gantt_chart_group()]),
            step("Supply vs Demand Charts", cards=[gc.get_supply_vs_demand_chart_group()]),
            step(
                "Availability Charts",
                cards=[
                    gc.combine_chart_groups(
                        app_session,
                        gc.get_processing_plant_avail_chart_group(
                            app_.limit_time_to_optimisation_bounds, show_chart_title=True
                        ),
                        gc.get_vehicle_avail_chart_group(show_chart_title=True),
                        title="Availability Charts",
                    )
                ],
            ),
            step("Train Schedule", cards=[card([base_wg.TrainScheduleTimeline()])]),
        ]

    @classmethod
    def get_marketing_step(cls, app_session: "AppSession") -> "Step":
        return step(
            "Marketing",
            cards=cls.bolt_grid_group(
                base_orm.Contract,
                base_orm.ContractLocation,
                base_orm.Product,
                base_orm.ProductAttribute,
                base_orm.SalesOrder,
            ),
        )


class OperationalOptimiserTabFactory(OptimiserTabFactory):
    pass


class OperationalOutputTabFactory(OutputTabFactory):
    @classmethod
    def get_output_chart_steps(cls, app_session: "AppSession") -> List["Step"]:
        return [
            step("Network Constraints", cards=[card([base_wg.BindingConstraintChart()])]),
            step("System Summary", cards=[card(base_wg.simple_system_summary_chart(app_session))]),
            step("Throughput Limit Charts", cards=[gc.get_throughputlimit_chart_group()]),
            step("Network Charts", cards=[gc.get_transported_sankey_chart_group()]),
            step("Transport Charts", cards=[gc.get_transported_from_to_chart_group()]),
            step("Mine Production", cards=[gc.get_mine_production_chart_group()]),
            step("Mining Schedule Charts", cards=[card([base_wg.MiningScheduleChart()])]),
            step("Block Supply Charts", cards=[gc.get_block_supply_used_chart_group()]),
            step("Unused Block Attributes", cards=[gc.get_unused_block_attributes_chart_group()]),
            step(
                "Stockyard Usage Charts",
                cards=[gc.get_stockyard_usage_chart_group(), gc.get_stockpile_usage_chart_group()],
            ),
            step("Storage Offtake Charts", cards=[gc.get_storage_offtake_chart_group()]),
            step(
                "Blend Schedule Charts",
                cards=[
                    gc.combine_chart_groups(
                        app_session,
                        gc.get_blend_schedule_block_chart_group(),
                        gc.get_blend_schedule_blend_chart_group(),
                        title="Blend Schedule Charts",
                    )
                ],
            ),
            step(
                "Processing Charts",
                cards=[
                    gc.combine_chart_groups(
                        app_session,
                        gc.get_blocks_processed_chart_group(),
                        gc.get_processing_plant_utilisation_chart_group(),
                    )
                ],
            ),
            step("Train Charts", cards=[card(base_wg.TrainsPerPeriod()), card(base_wg.RailPlanTimeline())]),
            step("Demand Filled Charts", cards=[gc.get_demand_filled_no_contract_chart_group()]),
            step("Vessel Attribute Charts", cards=[gc.get_vessel_attributes_chart_group()]),
            step("Opportunistic Attribute Charts", cards=[gc.get_opportunistic_attributes_chart_group()]),
            step("Vessel Demurrage Charts", cards=[gc.get_vessel_demmurage_chart_group()]),
            step("Revenue Charts", cards=[gc.get_revenue_chart_group()]),
        ]

    @classmethod
    def get_output_report_steps(cls, app_session: "AppSession") -> List["Step"]:
        return [
            step(
                "Export Reports",
                cards=[
                    card(
                        [
                            reports.StockpileResults(),
                            reports.TransportOperations(),
                            reports.WashReport(),
                            reports.ProcessingReport(),
                            reports.BonusFilledAttributesReport(),
                            reports.TransportedCommoditiesReport(),
                            reports.RailResultsReport(),
                            reports.KPIReport(),
                            reports.VesselDemandReport(),
                            reports.PortCosts(),
                            reports.ClosingStocksReport(),
                        ],
                        title="Export Reports",
                    )
                ],
            )
        ]

    def get_gui_tab(self, app_session: "AppSession") -> "StepTab":
        return step_tab(
            name=self.get_tab_name(),
            step_groups=[
                step_group(name="Review Output Charts", steps=self.get_output_chart_steps(app_session)),
                step_group(name="Export Reports", steps=self.get_output_report_steps(app_session)),
                self.get_lock_data_set_step_group(app_session),
            ],
        )


# Tactical
class TacticalInputTabFactory(InputTabFactory):
    @classmethod
    def get_production_step_group(cls, app_session: "AppSession") -> "StepGroup":
        from bolt_base.apps.optimiser_app_def import BoltOptimiserApp

        app_ = cast(BoltOptimiserApp, app_session.app)
        block_supply_grid = base_wg.BoltGrid(base_orm.BlockSupply)
        block_grid = base_wg.BoltGrid(base_orm.Block)
        block_attribute_grid = base_wg.BoltGrid(base_orm.BlockAttribute)
        block_yield_grid = base_wg.BoltGrid(base_orm.BlockYield)
        return step_group(
            name="Production",
            steps=[
                step(
                    name="Block Supplies",
                    cards=[
                        gc.get_tonnes_per_period_chart_group(
                            app_.limit_time_to_optimisation_bounds, subscribe_grid=[block_supply_grid, block_grid],
                        ),
                        card(block_supply_grid),
                        card(block_grid),
                    ],
                ),
                step(
                    name="Block Attributes",
                    cards=[
                        gc.get_processing_attributes_chart_group(subscribe_grid=[block_attribute_grid]),
                        card(block_attribute_grid),
                    ],
                ),
                step(
                    name="Processing Yield",
                    cards=[
                        gc.get_processing_yield_chart_group(subscribe_grid=[block_yield_grid]),
                        card(block_yield_grid),
                    ],
                ),
            ],
        )

    @classmethod
    def get_opening_stocks_step_group(cls, app_session: "AppSession") -> "StepGroup":
        closing_stocks_uploader = base_wg.ClosingStocksUploader()
        opening_mine_commodity_location_supply_grid = base_wg.BoltGrid(
            base_orm.OpeningMineCommodityLocationSupply, additional_hidden_cols=["supply_start_time", "supply_end_time"]
        )
        opening_mine_commodity_attribute_grid = base_wg.BoltGrid(base_orm.OpeningMineCommodityAttribute)
        opening_mine_commodity_yield_grid = base_wg.BoltGrid(base_orm.OpeningMineCommodityYield)
        opening_mine_commodity_grid = base_wg.BoltGrid(base_orm.OpeningMineCommodity)

        stock_target_grid = base_wg.BoltGrid(base_orm.StockyardCapacity)
        mine_product_stock_target = base_wg.BoltGrid(base_orm.StockyardMineProductCapacity)

        return step_group(
            name="Stockyards",
            steps=[
                step(
                    "Upload Opening Stocks",
                    [
                        card(closing_stocks_uploader),
                        gc.get_opening_stocks_charts_group(subscribe_uploader=[closing_stocks_uploader]),
                    ],
                ),
                step(
                    "Opening Stocks",
                    [
                        gc.get_opening_stocks_charts_group(
                            subscribe_grid=[
                                opening_mine_commodity_location_supply_grid,
                                opening_mine_commodity_attribute_grid,
                                opening_mine_commodity_yield_grid,
                                opening_mine_commodity_grid,
                            ]
                        ),
                        card(opening_mine_commodity_location_supply_grid, title="Opening Stocks"),
                        card(opening_mine_commodity_attribute_grid),
                        card(opening_mine_commodity_yield_grid),
                        card(opening_mine_commodity_grid),
                    ],
                ),
                step(
                    name="Stock Targets",
                    cards=[
                        gc.get_stockyard_chart_group(subscribe_grid=[stock_target_grid, mine_product_stock_target]),
                        card(stock_target_grid, title="Stock Targets"),
                        card(mine_product_stock_target, title="Mine Product Stock Targets"),
                    ],
                ),
            ],
        )

    @classmethod
    def get_marketing_step_group(cls, app_session: "AppSession") -> "StepGroup":
        contracted_demand_grid = base_wg.BoltGrid(base_orm.ContractedDemand)
        opportunistic_demand_grid = base_wg.BoltGrid(base_orm.OpportunisticDemand)
        vessel_demand_grid = base_wg.BoltGrid(base_orm.VesselDemand)
        sales_order_grid = base_wg.BoltGrid(base_orm.SalesOrder)

        pricing_index_price_grid = base_wg.BoltGrid(base_orm.PricingIndexPrice)
        pricing_index_grid = base_wg.BoltGrid(base_orm.PricingIndex)
        exchange_rate_grid = base_wg.BoltGrid(base_orm.ExchangeRate)
        currency_grid = base_wg.BoltGrid(base_orm.Currency)

        currency_filter = base_wg.CurrencyFilter()
        return step_group(
            name="Marketing",
            steps=[
                step(name="Vessel Arrival", cards=[gc.get_vessel_gantt_chart_group()]),
                step(
                    name="Demands",
                    cards=[
                        gc.combine_chart_groups(
                            app_session,
                            gc.get_contracted_demand_chart_group(
                                subscribe_grid=[contracted_demand_grid, sales_order_grid]
                            ),
                            [contracted_demand_grid],
                        ),
                        gc.combine_chart_groups(
                            app_session,
                            gc.get_opportunistic_demand_chart_group(
                                subscribe_grid=[opportunistic_demand_grid, sales_order_grid]
                            ),
                            [opportunistic_demand_grid],
                        ),
                        gc.combine_chart_groups(
                            app_session,
                            gc.get_vessel_demand_chart_group(subscribe_grid=[vessel_demand_grid, sales_order_grid]),
                            [vessel_demand_grid],
                        ),
                        card([sales_order_grid]),
                    ],
                ),
                step(
                    name="Prices",
                    cards=[
                        card(
                            [
                                currency_filter,
                                base_wg.PricingIndexPriceChart(
                                    filter_=currency_filter,
                                    subscribe_grid=[
                                        pricing_index_price_grid,
                                        pricing_index_grid,
                                        exchange_rate_grid,
                                        currency_grid,
                                    ],
                                ),
                            ]
                        ),
                        card(pricing_index_price_grid, title="Prices"),
                        card(pricing_index_grid),
                        card(
                            base_wg.ExchangeRateChart(
                                subscribe_grid=[
                                    pricing_index_grid,
                                    pricing_index_price_grid,
                                    exchange_rate_grid,
                                    currency_grid,
                                ]
                            )
                        ),
                        card(exchange_rate_grid),
                        card(currency_grid),
                    ],
                ),
                step(name="Contracts", cards=cls.bolt_grid_group(base_orm.Contract, base_orm.ContractLocation)),
                step(name="Products", cards=cls.bolt_grid_group(base_orm.Product, base_orm.ProductAttribute)),
                step(name="Vessels", cards=cls.bolt_grid_group(base_orm.Vessel, base_orm.Shipment)),
            ],
        )

    @classmethod
    def get_rate_step_group(cls, app_session: "AppSession") -> "StepGroup":
        from bolt_base.apps.optimiser_app_def import BoltOptimiserApp

        app_ = cast(BoltOptimiserApp, app_session.app)
        location_availability_grid = base_wg.BoltGrid(base_orm.LocationAvailability)
        transport_route_availability_grid = base_wg.BoltGrid(base_orm.TransportRouteAvailability)

        transport_route_filter = base_wg.TransportRouteNameFilter()

        return step_group(
            name="Throughput Limits",
            steps=[
                step(
                    name="Availability",
                    cards=[
                        gc.get_processing_plant_avail_chart_group(
                            app_.limit_time_to_optimisation_bounds,
                            subscribe_grid=[location_availability_grid, transport_route_availability_grid],
                        ),
                        card(location_availability_grid),
                        card(
                            [
                                transport_route_filter,
                                base_wg.TransportRouteAvailabilityChart(
                                    filter_=transport_route_filter, subscribe_grid=[transport_route_availability_grid]
                                ),
                            ]
                        ),
                        card(transport_route_availability_grid, title="Route Availability"),
                        card(base_wg.BoltGrid(base_orm.VehicleAvailability)),
                    ],
                ),
                step(
                    name="Location Limits",
                    cards=[
                        card(base_wg.BoltGrid(base_orm.ThroughputLimit), title="Location Throughput Limit"),
                        card(
                            base_wg.BoltGrid(base_orm.ThroughputLimitCapacity),
                            title="Location Throughput Limit Capacity",
                        ),
                        card(base_wg.BoltGrid(base_orm.ThroughputTarget)),
                    ],
                ),
                step(
                    "Route Limits",
                    cls.bolt_grid_group(
                        base_orm.RouteThroughputLimit,
                        base_orm.RouteThroughputLimitCommodity,
                        base_orm.RouteThroughputLimitCapacity,
                        base_orm.RouteThroughputLimitCapacityCommodity,
                    ),
                ),
            ],
        )

    @classmethod
    def get_configuration_step_group(cls, app_session: "AppSession") -> "StepGroup":
        return step_group(
            name="Configuration",
            steps=[
                step(
                    "Locations",
                    cls.bolt_grid_group(
                        base_orm.Pit, base_orm.ProcessingPlant, base_orm.TrainLoadOut, base_orm.ShippingTerminal
                    ),
                ),
                step(name="Stockyards", cards=cls.bolt_grid_group(base_orm.RawStockyard, base_orm.CleanStockyard)),
                step("Transports", cls.bolt_grid_group(base_orm.TransportRoute, base_orm.TransportRouteCommodityGroup)),
                step(
                    "Mine Products",
                    cls.bolt_grid_group(
                        base_orm.MineProduct,
                        base_orm.MineProductAttributeBounds,
                        base_orm.MineCommodityMineProductMapping,
                    ),
                ),
                step("Blending", cls.bolt_grid_group(base_orm.Blend, base_orm.BlendInput)),
                step(
                    "Processing Configuration",
                    cls.bolt_grid_group(
                        base_orm.ProcessingConfiguration,
                        base_orm.ProcessingType,
                        base_orm.ProcessingConfigurationType,
                        base_orm.ProcessingConfigurationMineCommodity,
                    ),
                ),
            ]
            + cls.get_colour_scheme_steps(app_session),
        )

    def get_gui_tab(self, app_session: "AppSession") -> "StepTab":
        return step_tab(name=self.get_tab_name(), step_groups=self.get_step_groups(app_session))

    @classmethod
    def get_step_groups(cls, app_session: "AppSession") -> List["StepGroup"]:
        return [
            cls.get_production_step_group(app_session),
            cls.get_opening_stocks_step_group(app_session),
            cls.get_marketing_step_group(app_session),
            cls.get_rate_step_group(app_session),
            cls.get_input_chart_step_group(app_session),
            cls.get_configuration_step_group(app_session),
        ]

    @classmethod
    def get_input_chart_step_group(cls, app_session: "AppSession") -> "StepGroup":
        return step_group(
            name="Review",
            steps=[
                step("Network", [gc.get_network_chart_group()]),
                step("Supply vs Demand", [gc.get_supply_vs_demand_chart_group()]),
            ],
        )


class TacticalOptimiserTabFactory(OptimiserTabFactory):
    pass


class TacticalOutputTabFactory(OutputTabFactory):
    @classmethod
    def get_summary_chart_step_groups(cls, app_session: "AppSession") -> "StepGroup":
        return step_group(
            name="Summary",
            steps=[
                step("Network Constraints", cards=[card([base_wg.BindingConstraintChart()])]),
                step("System Summary", cards=[card(base_wg.simple_system_summary_chart(app_session))]),
            ],
        )

    @classmethod
    def get_production_step_group(cls, app_session: "AppSession") -> "StepGroup":
        return step_group(
            name="Production",
            steps=[
                step("Block Supply", cards=[gc.get_block_supply_used_chart_group()]),
                step("Unused Block Attributes", cards=[gc.get_unused_block_attributes_chart_group()]),
                step(
                    "Processing",
                    cards=[
                        gc.combine_chart_groups(
                            app_session,
                            gc.get_blocks_processed_chart_group(),
                            gc.get_processing_plant_utilisation_chart_group(),
                            title="Processing Charts",
                        )
                    ],
                ),
                step("Mine Production", cards=[gc.get_mine_production_chart_group()]),
                step("Mining Schedule", cards=[card([base_wg.MiningScheduleChart()])]),
            ],
        )

    @classmethod
    def get_marketing_step_group(cls, app_session: "AppSession") -> "StepGroup":
        return step_group(
            name="Marketing",
            steps=[
                step("Demand Filled", cards=[gc.get_demand_filled_chart_group()]),
                step("Vessel Attribute", cards=[gc.get_vessel_attributes_chart_group()]),
                step("Contracted Attribute", cards=[gc.get_contracted_attributes_chart_group()]),
                step("Opportunistic Attribute", cards=[gc.get_opportunistic_attributes_chart_group()]),
                step(
                    "Blending",
                    cards=[
                        gc.combine_chart_groups(
                            app_session,
                            gc.get_blend_schedule_block_chart_group(),
                            gc.get_blend_schedule_blend_chart_group(),
                            title="Blend Schedule Charts",
                        )
                    ],
                ),
                step("Revenue", cards=[gc.get_revenue_chart_group()]),
            ],
        )

    @classmethod
    def get_network_step_group(cls, app_session: "AppSession") -> "StepGroup":
        return step_group(
            name="Network",
            steps=[
                step("Network Usage", cards=[gc.get_transported_sankey_chart_group()]),
                step("Throughput Usage", cards=[gc.get_throughputlimit_chart_group()]),
                step("Transport", cards=[gc.get_transported_from_to_chart_group()]),
                step(
                    "Stockyard Usage",
                    cards=[gc.get_stockyard_usage_chart_group(), gc.get_stockpile_usage_chart_group()],
                ),
                step("Storage Offtake", cards=[gc.get_storage_offtake_chart_group()]),
                step("Vessel Demurrage", cards=[gc.get_vessel_demmurage_chart_group()]),
            ],
        )

    @classmethod
    def get_network_step_group(cls, app_session: "AppSession") -> "StepGroup":
        return step_group(
            name="Network",
            steps=[
                step("Network Usage", cards=[gc.get_transported_sankey_chart_group()]),
                step("Throughput Usage", cards=[gc.get_throughputlimit_chart_group()]),
                step("Transport", cards=[gc.get_transported_from_to_chart_group()]),
                step(
                    "Stockyard Usage",
                    cards=[gc.get_stockyard_usage_chart_group(), gc.get_stockpile_usage_chart_group()],
                ),
                step("Storage Offtake", cards=[gc.get_storage_offtake_chart_group()]),
                step("Vessel Demurrage", cards=[gc.get_vessel_demmurage_chart_group()]),
            ],
        )

    @classmethod
    def get_output_report_steps(cls, app_session: "AppSession") -> List["Step"]:
        return [
            step(
                "Individual Reports",
                cards=[
                    card(
                        [
                            reports.BlendScheduleReport(),
                            reports.BonusFilledAttributesReport(),
                            reports.CargoSchedule(),
                            reports.CargoSummary(),
                            reports.ClosingStocksReport(),
                            reports.KPIReport(),
                            reports.MineProductsProcessedReport(),
                            reports.OpeningStocksTemplateReport(),
                            reports.OpexPerTonneReport(),
                            reports.OppContractedDemandReport(),
                            reports.PortCosts(),
                            reports.PortOperation(),
                            reports.PricingIndexReport(),
                            reports.ProcessingReport(),
                            reports.ProductQualityReport(),
                            reports.RailPlanReport(),
                            reports.ShipmentDetails(),
                            reports.ShipmentSummary(),
                            reports.ShortTermWashDemand(),
                            reports.StockpileResults(),
                            reports.TonneScheduleReport(),
                            reports.TransportOperations(),
                            reports.TransportedCommoditiesReport(),
                            reports.RailResultsReport(),
                            reports.VesselDemandReport(),
                            reports.WashReport(),
                        ],
                        title="Reports",
                    )
                ],
            ),
        ]

    def get_gui_tab(self, app_session: "AppSession") -> "StepTab":
        return step_tab(
            name=self.get_tab_name(),
            step_groups=[
                self.get_summary_chart_step_groups(app_session),
                self.get_production_step_group(app_session),
                self.get_marketing_step_group(app_session),
                self.get_network_step_group(app_session),
                step_group(name="Export Reports", steps=self.get_output_report_steps(app_session)),
                self.get_lock_data_set_step_group(app_session),
            ],
        )
