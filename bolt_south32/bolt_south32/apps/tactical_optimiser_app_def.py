import datetime as dt
import collections
import logging

from dateutil import relativedelta as rd

from tropofy.ext.datetime_window import DateTimeWindow

from bolt_base.apps.optimiser_app_def import BoltOptimiserApp
from bolt_base.models import orm, parameters
from bolt_base.utils import time_step as time_step_utils

from bolt_south32.apps._base import South32AppMixin
from bolt_south32.apps.south32_tab_factory import (
    TacticalInputTabFactory,
    TacticalOptimiserTabFactory,
    TacticalOutputTabFactory,
)

from typing import List, TYPE_CHECKING

if TYPE_CHECKING:
    from tropofy.app.meta import AppSession
    from tropofy.app.data_set import DataSet


class TacticalApp(BoltOptimiserApp, South32AppMixin):
    @classmethod
    def get_start_datetime(cls, data_set: "DataSet") -> dt.datetime:
        return parameters.BoltOptimisationParameters.optimisation_start_time.get_value(data_set)

    @classmethod
    def get_end_datetime(cls, data_set: "DataSet") -> dt.datetime:
        return parameters.BoltOptimisationParameters.optimisation_end_time.get_value(data_set)

    @classmethod
    def get_time_steps(
        cls, data_set: "DataSet", start_datetime: dt.datetime, end_datetime: dt.datetime
    ) -> List[DateTimeWindow]:
        interval: rd.relativedelta = parameters.BoltOptimisationParameters.optimisation_step_interval.get_value(
            data_set
        )
        return time_step_utils.get_uniform_period_time_steps_truncated_at_end_time(
            start_datetime, end_datetime, interval
        )

    def get_default_end(self, app_session: "AppSession"):
        # End of month + 12 months
        return self.get_default_start(app_session) + rd.relativedelta(months=12)

    def max_optimisation_length(self, app_session: "AppSession"):
        return rd.relativedelta(weeks=60)

    def get_optimisation_dates(self, app_session: "AppSession", master_data_set, requested_start, requested_end):
        # set shift start to midnight so demands align properly
        start = dt.datetime.combine(requested_start, self.SINGLE_SHIFT_START_TIME)
        optimisation_end = truncated_end = requested_end
        optimisation_end = dt.datetime.combine(optimisation_end.date(), self.SINGLE_SHIFT_START_TIME)
        return collections.namedtuple("Dates", ["optimisation_start", "optimisation_end", "truncated_end"])(
            start, optimisation_end, truncated_end
        )

    def post_process_data_set(self, app_session: "AppSession", master_data_set, logger: logging.Logger, **kwargs):
        parameters.BoltOptimisationParameters.optimisation_step_interval.set_value(
            app_session.data_set, rd.relativedelta(weeks=1)
        )
        self.set_single_shift_with_start_time(app_session.data_set, self.SINGLE_SHIFT_START_TIME)

    def get_table_names_not_to_copy_from_master_data(self):
        return [
            orm.TakeOrPayContract.__table__.name,
            orm.TakeOrPayRoute.__table__.name,
        ] + super().get_table_names_not_to_copy_from_master_data()

    def _get_auto_load_data_set_id(self, app_session: "AppSession"):
        return None

    # noinspection PyTypeChecker
    def get_tab_factories(self):
        return [
            TacticalInputTabFactory(),
            TacticalOptimiserTabFactory(),
            TacticalOutputTabFactory(),
        ] + super().get_tab_factories()
