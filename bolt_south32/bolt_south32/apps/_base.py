from bolt_base.apps.base import BaseApp
from bolt_south32.apps.south32_tab_factory import South32MasterTabFactory


class South32AppMixin(BaseApp):
    def get_master_tab_factory(self):
        return South32MasterTabFactory()

    def get_package_name(self) -> str:
        return "tropofy-bolt-south32"
