from setuptools import setup, find_packages
import os
import warnings

requires = ["tropofy-bolt-base~=4.26.4"]


here = os.path.abspath(os.path.dirname(__file__))

try:
    with open(os.path.join(here, "version.txt"), "r") as f:
        version = f.read().strip()
except IOError:
    warnings.warn("No version file found. Defaulting to 0.0.0")
    version = "0.0.0"


setup(
    name="tropofy-bolt-south32",
    version=version,
    description="Commodity Blending through Supply Chain Network",
    long_description="This app is the property of Polymathian Pty Ltd.",
    classifiers=["Programming Language :: Python"],
    author="Polymathian Pty Ltd",
    author_email="info@polymathian.com",
    url="http://www.polymathian.com",
    license="This app is the property of Polymathian Pty Ltd.",
    keywords="tropofy, polymathian",
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=requires,
)
